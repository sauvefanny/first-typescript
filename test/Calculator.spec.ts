import {Calculator} from '../src/exo/calculator';

describe('Calculator class', ()=>{
    it ('should multiply two values', ()=>{
        const calculator = new Calculator();
        const result = calculator.calculate(10, 2, '*');
        expect(result).toBe(20);
    })
})

