// import { Person } from "./NormalHuman";


// function maFonction(text: string): number{

//  return text.length ;
// }

// let person1 = new Person('bloup', 34);
// let person2 = new Person('blip', 31);

// console.log(person1.greet(person2));
// console.log(person2.greet(person1));
import { server } from "./server";
import 'dotenv-flow/config'
import { createConnection, getRepository } from "typeorm";
import { Person } from "./entity/Person";

createConnection({
    type: "mysql",
    url: process.env.DATABASE_URL,
    synchronize: true,
    entities: ['src/entity/*.ts']
});

const port:number = Number(process.env.PORT) || 8000;

server.listen(port, () =>{
    console.log(`listening on port ${port}`)
});
