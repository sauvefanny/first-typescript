import { Request, Router } from "express";
import { getRepository } from "typeorm";
import { Person } from "../entity/Person";

export const personneController = Router();

personneController.get('/', async (req, res) => {
    try {
        const persons = await getRepository(Person).find();
        res.json(persons);
    } catch (error) {
        console.log(error);
        res.status(500).json(error);

    }

});

personneController.get('/:id', async (req, res) => {
    try {
        const person = await getRepository(Person).findOne(req.params.id)
        if(!person){
            res.status(404).end();
            return;
        }
        res.json(person)
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

})


personneController.post('/', async (req, res) => {
    try {
        let person = new Person();
        Object.assign(person, req.body);

        await getRepository(Person).save(person);

        res.status(201).json(person)

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }

})




