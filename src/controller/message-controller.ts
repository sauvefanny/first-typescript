import { Router } from "express";
import { Request } from "express-serve-static-core";

export const messageController = Router();

interface Message{
    sender:string;
    content:string;
    date?: Date| string;
};

const messages: Message[] = [];
messages.push({content:'machin', sender:'bidule'});

messageController.get('/', (req, res)=>{
    res.json(messages);
});

messageController.post('/', (req: Request<{}, {}, Message>, res)=>{
    req.body
    req.params
    req.body.date = new Date();
    messages.push(req.body);
    res.status(201).json(req.body);
})