import express from "express";
import cors from 'cors';
import {messageController} from './controller/message-controller';
import { personneController } from "./controller/personne-controller";


export const server = express()
const router = express.Router();


router.get('/', (req, res)=>{
    res.send('Hello world');

});

server.use(express.json());
server.use(cors)

server.use('/api/message', messageController);
server.use('/api/personne', personneController);

