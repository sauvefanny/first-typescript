export class Calculator{

    calculate(a: number, b: number, operator: string): number{
        switch (operator){
            case "+":
                return a+b;
            case "-":
                return a-b;
            case "*":
                return a*b;
            case "/":
                return a/b;
            default:
                throw new Error ("wrong operator");
        }
    }
}