export class NormalHuman {
    // public name : string;// public
    // private age: number;// private
    constructor(
        public name: string,
        private age: number
    ) { }

    greet(person: NormalHuman): string {

        return `Hello ${person.name}, I am ${this.name} years old`

    }

}